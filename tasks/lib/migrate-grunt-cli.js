
var Knex = require('knex'),

    knex;

module.exports = function(grunt, config) {
  if (!config.db) { throw new Error('config `db` is not specified'); }
  knex = Knex(config.db);

  var commands = {

    latest: function() {
      return knex.migrate.latest(config)
        .spread(function(batchNo, log) {
          if (log.length === 0) {
            grunt.log.write('Already up to date');
          } else {
            grunt.log.oklns('Batch %s run: %d migrations', batchNo, log.length);
          }
        })
        .catch(logError);
    },

    make: function(name) {
      if (!name) { return grunt.log.error('The name must be defined'); }

      return knex.migrate.make(name, config)
        .then(function(filename) {
          grunt.log.ok('Migration ' + filename + ' created!');
        })
        .catch(logError);
    }

  };

  function logError(e) {
    grunt.log.error(e);
  }

  return commands;
};

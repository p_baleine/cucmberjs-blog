
/**
 * migration tasks
 */

// grunt migrate:latest
// => run knex:migrate:latest on development env

// grunt env:unit migrate:latest
// => run knex:migrate:latest on unit env

// grunt migrate:make --name=create_posts
// => run knex:migrate:make create_posts

var _ = require('underscore'),
    gruntCli = require('./lib/migrate-grunt-cli');

module.exports = function(grunt) {
  grunt.registerTask('migrate', function(commandName) {
    var done = this.async(),
        config = require('config'),
        isDebug = config.db.debug,
        name = grunt.option('name');

    config.db.debug = false;

    gruntCli(grunt, config)[commandName](name).then(function() {
      config.db.debug = isDebug;
      done();
    });
  });
};

var _ = require('underscore'),
    expect = require('chai').expect,
    ValidatorError = require('validator').ValidatorError,
    models = require('../../../app/models'),
    Bookshelf = require('bookshelf').blogBookshelf,
    renderView = require('../../utils/render-view');

describe('post/new view', function() {

  function renderNewView(locals) {
    var defaultParams = {
      post: {},
      tags: [
        { id: 1, text: 'hello' },
        { id: 2, text: 'world' }
      ]
    };

    return renderView('app/views/post/new.jade', _.extend(defaultParams, locals));
  }

  it('should render new post form.', function(done) {
    renderNewView().then(function(window) {
      expect(window.$('form').length).to.equal(1);
      expect(window.$('form').attr('action')).to.equal('/posts');
      expect(window.$('form').attr('method')).to.equal('post');
    }).then(done).catch(done);
  });

  it('should render text input for new post\'s title.', function(done) {
    renderNewView().then(function(window) {
      expect(window.$('input[name="post[title]"]').length).to.equal(1);
      expect(window.$('input[name="post[title]"]').attr('type')).to.equal('text');
    }).then(done).catch(done);
  });

  it('should render textarea for new post\'s description.', function(done) {
    renderNewView().then(function(window) {
      expect(window.$('textarea[name="post[description]"]').length).to.equal(1);
    }).then(done).catch(done);
  });

  it('should render submit button to save new post.', function(done) {
    renderNewView().then(function(window) {
      expect(window.$('input[type="submit"]').length).to.equal(1);
    }).then(done).catch(done);
  });

  it('should render error if given.', function(done) {
    renderNewView({ error: new ValidatorError(['some error']) }).then(function(window) {
      expect(window.$('.error').html()).to.contain('some error');
    }).then(done).catch(done);
  });

  it('should render tag checkboxes.', function(done) {
    renderNewView().then(function(window) {
      expect(window.$('form .tags [type=checkbox]').length).to.equal(2);
      expect(window.$('form .tags [type=checkbox]:first-child').attr('name'))
        .to.equal('post[tags]');
      expect(window.$('form .tags [type=checkbox]:first-child').val())
        .to.equal('1');
      expect(window.$('form .tags label:first-child').text())
        .to.equal('hello');
    }).then(done).catch(done);
  });

});

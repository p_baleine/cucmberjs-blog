var _ = require('underscore'),
    expect = require('chai').expect,
    renderView = require('../../utils/render-view');

describe('post/edit view', function() {

  function renderEditView(locals) {
    var defaultParams = {
      post: {
        id: 11,
        title: 'My title',
        description: 'My description',
        tags: [{ id: 1 }]
      },
      tags: [
        { id: 1, text: 'hello' },
        { id: 2, text: 'world' }
      ]
    };

    return renderView('app/views/post/edit.jade', _.extend(defaultParams, locals));
  }

  it('should edit view post form.', function(done) {
    renderEditView().then(function(window) {
      expect(window.$('form').length).to.equal(1);
      expect(window.$('form').attr('action')).to.equal('/posts/' + 11);
      expect(window.$('form').find('[name=_method]').val()).to.equal('put');
    }).then(done).catch(done);
  });

  it('should check tag checkbox to which the post attached.', function(done) {
    renderEditView().then(function(window) {
      expect(window.$('[name*=tags][value="1"]').is(':checked')).to.be.ok;
    }).then(done).catch(done);
  });

});

var expect = require('chai').expect,
    moment = require('moment'),
    models = require('../../../app/models'),
    Bookshelf = require('bookshelf').blogBookshelf,
    fixture = require('../../../fixtures'),
    viewHelpers = require('../../../app/view-helpers'),
    renderView = require('../../utils/render-view');

var createdAt = new Date();

// TODO comments fixture

describe('post/show view', function() {

  function renderShowView() {
    var post = fixture.posts[0];

    post.created_at = createdAt;
    post.comments = [{ commenter: 'hoge' }, { commenter: 'piyo' }];
    post.tags = fixture.tags;

    return renderView('app/views/post/show.jade', { post: post });
  }

  it('should render post title', function(done) {
    renderShowView().then(function(window) {
      expect(window.$('.title').text()).to.equal(fixture.posts[0].title);
    }).then(done).catch(done);
  });

  it('should render post description', function(done) {
    renderShowView().then(function(window) {
      expect(window.$('.description').text()).to.equal(fixture.posts[0].description);
    }).then(done).catch(done);
  });

  it('should render post created_at', function(done) {
    renderShowView().then(function(window) {
      expect(window.$('.created_at').text())
        .to.equal(moment(createdAt).format());
    }).then(done).catch(done);
  });

  it('should render form for comment.', function(done) {
    renderShowView().then(function(window) {
      expect(window.$('form')).to.have.length(1);
      expect(window.$('form').attr('method')).to.equal('POST');
      expect(window.$('form').attr('action'))
        .to.equal('/posts/' + fixture.posts[0].id + '/comments');
      expect(window.$('form').find('[name="comment[commenter]"]')).to.have.length(1);
      expect(window.$('form').find('[name="comment[body]"]')).to.have.length(1);
    }).then(done).catch(done);
  });

  it('should render link for edit this post', function(done) {
    renderShowView().then(function(window) {
      expect(window.$('a[href="/posts/' + fixture.posts[0].id + '/edit"]'))
        .to.have.length(1);
    }).then(done).catch(done);
  });
});

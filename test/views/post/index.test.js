var expect = require('chai').expect,
    moment = require('moment'),
    models = require('../../../app/models'),
    Bookshelf = require('bookshelf').blogBookshelf,
    fixture = require('../../../fixtures').posts,
    viewHelpers = require('../../../app/view-helpers'),
    renderView = require('../../utils/render-view');

var createdAt = new Date();

describe('post/index view', function() {

  function renderIndexView() {
    var posts = fixture;

    posts.forEach(function(post) {
      post.created_at = createdAt;
      post.comments = [{ commenter: 'hoge' }, { commenter: 'piyo' }];
      post.tags = [{ text: 'hello' }, { text: 'goodbye' }];
    });

    return renderView('app/views/post/index.jade', { posts: posts });
  }

  it('should render post index', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('#posts').length).to.equal(1);
    }).then(done).catch(done);
  });

  it('should render each post item', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('.post').length).to.equal(fixture.length);
    }).then(done).catch(done);
  });

  it('should render post item title', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('.post:first-child').find('.title').text())
        .to.equal(fixture[0].title);
    }).then(done).catch(done);
  });

  it('should render post item description', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('.post:first-child').find('.description').text())
        .to.equal(fixture[0].description);
    }).then(done).catch(done);
  });

  it('should render post item\'s detail link.', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('.post:first-child').find('a').attr('href'))
        .to.equal('/posts/' + fixture[0].id);
    }).then(done).catch(done);
  });

  it('should render post item\'s created time', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('.post:first-child').find('.created_at').text())
        .to.equal(moment(createdAt).format());
    }).then(done).catch(done);
  });

  it('should render link to \'/new\'', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('a[href="/posts/new"]').length).to.equal(1);
    }).then(done).catch(done);
  });

  it('should render tags.', function(done) {
    renderIndexView().then(function(window) {
      expect(window.$('.post:first-child').find('.tags .tag:first-child').text())
        .to.equal('hello');
    }).then(done).catch(done);
  });

});

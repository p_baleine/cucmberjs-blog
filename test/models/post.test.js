var _ = require('underscore'),
    expect = require('chai').expect,
    sinon = require('sinon'),
    knex = require('bookshelf').blogBookshelf.knex,
    Post = require('../../app/models').Post,
    Comment = require('../../app/models').Comment,
    Tag = require('../../app/models').Tag;

describe('Post model', function() {

  describe('relation', function() {
    describe('#comments', function() {
      beforeEach(function() {
        this.post = new Post();
        this.comments = this.post.comments();
      });

      it('should return Comments instance', function() {
        expect(this.comments.model).to.equal(Comment);
      });

      it('should return Comments related to posts', function() {
        expect(this.comments.relatedData.parentTableName).to.equal(this.post.tableName);
      });

      it('should return comments related to posts via `hasMany` relation', function() {
        expect(this.comments.relatedData.type).to.equal('hasMany');
      });
    });

    describe('#tags', function() {
      beforeEach(function() {
        this.post = new Post();
        this.tags = this.post.tags();
      });

      it('should return Comments instance', function() {
        expect(this.tags.model).to.equal(Tag);
      });

      it('should return Comments related to posts', function() {
        expect(this.tags.relatedData.parentTableName).to.equal(this.post.tableName);
      });

      it('should return comments related to posts via `belongsToMany` relation', function() {
        expect(this.tags.relatedData.type).to.equal('belongsToMany');
      });
    });
  });

  describe('#save', function() {
    beforeEach(function() {
      this.post = new Post({
        title: 'hello',
        description: 'world',
        tags: [1, 2]
      });
    });

    describe('when post is new,', function() {
      it('should attch tags.', function(done) {
        this.post.save().then(function(post) {
          knex('posts_tags').where('post_id', post.id).select()
            .then(function(postTags) {
              expect(_.pluck(postTags, 'tag_id')).to.have.members([1, 2]);
            })
            .then(done)
            .catch(done);
          });
      });
    });

    describe('when post is updated to change attached tags,', function() {
      it('should update attached tags.', function(done) {
        this.post.save().then(function(post) {
          post.set('tags', [1, 3]);
          post.save().then(function(post) {
            knex('posts_tags').where('post_id', post.id).select()
              .then(function(postTags) {
                expect(_.pluck(postTags, 'tag_id')).to.have.members([1, 3]);
                expect(_.pluck(postTags, 'tag_id')).to.not.include(2);
              })
              .then(done)
              .catch(done);
          });
        });
      });
    });
  });

  describe('timestamp', function() {
    it('should save created_at.', function(done) {
      new Post({ title: 'abc', description: 'def' }).save().then(function(post) {
        expect(post.get('created_at')).to.exist;
      }).then(done);
    });
  });

  describe('validation', function() {
    describe('when saving post', function() {
      it('should throw error when the title is empty', function(done) {
        var onSuccess = sinon.spy();

        new Post({ title: '', description: '' }).save().then(onSuccess, function(e) {
          expect(onSuccess).to.not.have.been.called;
          expect(e.name).to.equal('ValidatorError');
        }).then(done);
      });
    });
  });
});


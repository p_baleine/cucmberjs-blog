var expect = require('chai').expect,
    sinon = require('sinon'),
    Comment = require('../../app/models').Comment;

describe('Comment model', function() {

  describe('validation', function() {
    describe('when saving comment', function() {
      it('should throw error when the commenter is empty', function(done) {
        var onSuccess = sinon.spy();

        new Comment({ commenter: '', post_id: 1 }).save().then(onSuccess, function(e) {
          expect(onSuccess).to.not.have.been.called;
          expect(e.name).to.equal('ValidatorError');
        }).then(done);
      });
    });
  });
});


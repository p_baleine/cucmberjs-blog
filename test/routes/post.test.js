var chai = require('chai'),
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    models = require('../../app/models'),
    Bookshelf = require('bookshelf').blogBookshelf,
    post = require('../../app/routes/post'),
    fixture = require('../../fixtures').posts,
    ValidatorError = require('validator').ValidatorError,
    expect = chai.expect;

var mockModels = require('../utils/mock-models');

chai.use(sinonChai);

describe('post route', function() {

  beforeEach(function() {
    this.mockModels = mockModels();
  });

  afterEach(function() {
    this.mockModels.restore();
  });

  beforeEach(function() {
    this.res = { render: sinon.spy(), redirect: sinon.spy() };
    this.req = null;
  });

  describe('#index', function() {
    it('should render posts with post/index view.', function() {
      post.index(this.req, this.res);

      expect(this.res.render).to.have.been.calledWith('post/index');
    });

    it('should pass posts to post/index view.', function() {
      post.index(this.req, this.res);

      expect(this.res.render.lastCall.args[1]).to.be.exist;
      expect(this.res.render.lastCall.args[1]).to.have.property('posts');
    });

    it('should fetch with related tags.', function() {
      post.index(this.req, this.res);

      expect(this.mockModels.Post.findAllStub.lastCall.args[0])
        .to.have.property('withRelated');
      expect(this.mockModels.Post.findAllStub.lastCall.args[0].withRelated)
        .to.include('tags');
    });
  });

  describe('#new', function() {
    it('should render post/new view.', function() {
      post.new(this.req, this.res);

      expect(this.res.render).to.have.been.calledWith('post/new');
    });
  });

  describe('#edit', function() {
    beforeEach(function() {
      var model = new Bookshelf.Model(fixture[0]);

      sinon.stub(model, 'related', function() {
        return new Bookshelf.Collection();
      });

      this.mockModels.Post.fetchStub.returns({
        then: function(cb) {
          cb(model);
        }
      });
      this.req = { params: { post: 12 } };
    });


    describe('when specified post exists', function() {
      it('should render post/edit view.', function() {
        post.edit(this.req, this.res);

        expect(this.res.render).to.have.been.calledWith('post/edit');
      });

      it('should pass post to post/edit view', function() {
        post.edit(this.req, this.res);

        expect(this.mockModels.Post.fetchStub.thisValues[0].id).to.equal(12);
      });
    });

    describe('when specified post dose not exist', function() {
      beforeEach(function() {
        this.mockModels.Post.fetchStub.returns({
          then: function(onSuccess, onError) {
            return onError(new Error('EmptyResponse'));
          }
        });
        this.next = sinon.spy();
      });

      it('should set response statusCode 404.', function() {
        post.edit(this.req, this.res, this.next);

        expect(this.res.statusCode).to.equal(404);
      });

      it('call next with error.', function() {
        post.edit(this.req, this.res, this.next);

        expect(this.next).to.have.been.called;
        expect(this.next.lastCall.args[0]).to.be.an.instanceOf(Error);
      });
    });
  });

  describe('#create', function() {
    beforeEach(function() {
      this.req = { body: { post: { title: 'title', description: 'desc' } } };
    });

    it('should save passed title', function() {
      post.create(this.req, this.res);

      expect(this.mockModels.Post.saveStub)
        .to.have.been.called;
      expect(this.mockModels.Post.saveStub.thisValues[0].get('title'))
        .to.equal(this.req.body.post.title);
    });

    it('should save passed description', function() {
      post.create(this.req, this.res);

      expect(this.mockModels.Post.saveStub.thisValues[0].get('description'))
        .to.equal(this.req.body.post.description);
    });

    describe('when the new post is created successfully, ', function() {
      it('should redirect to posts index.', function() {
        post.create(this.req, this.res);

        expect(this.res.redirect).to.have.been.calledWith('/posts');
      });
    });

    describe('when the new post is not created by validation error', function() {
      beforeEach(function() {
        this.mockModels.Post.saveStub.returns({
          then: function(onSuccess, onError) {
            onError(new ValidatorError(['invaid']));
          }
        });
      });

      it('should render new with post model', function() {
        post.create(this.req, this.res);

        expect(this.res.render).to.have.been.calledWith('post/new');
        expect(this.res.render.lastCall.args[1]).to.have.property('post');
      });
    });
  });

  describe('#update', function() {
    it('should be a function', function() {
      expect(post.update).to.be.a('function');
    });
  });

  describe('#show', function() {
    beforeEach(function() {
      this.req = { params: ['1'] };
    });

    describe('when specified post exists', function() {
      it('should render post/show.', function() {
        post.show(this.req, this.res);

        expect(this.res.render).to.have.been.calledWith('post/show');
      });

      it('should pass post to post/show view.', function() {
        post.show(this.req, this.res);

        expect(this.res.render.lastCall.args[1]).to.be.exist;
        expect(this.res.render.lastCall.args[1]).to.have.property('post');
        expect(this.res.render.lastCall.args[1].post.title)
          .to.equal(fixture[0].title);
      });

      it('should fetch with related comments.', function() {
        post.show(this.req, this.res);

        expect(this.mockModels.Post.fetchStub.lastCall.args[0])
          .to.have.property('withRelated');
        expect(this.mockModels.Post.fetchStub.lastCall.args[0].withRelated)
          .to.include('comments');
      });

      it('should pass `require` true for `fetch` parameter.', function() {
        post.show(this.req, this.res);

        expect(this.mockModels.Post.fetchStub.lastCall.args[0])
          .to.have.property('require', true);
      });

      it('should fetch with related tags.', function() {
        post.show(this.req, this.res);

        expect(this.mockModels.Post.fetchStub.lastCall.args[0])
          .to.have.property('withRelated');
        expect(this.mockModels.Post.fetchStub.lastCall.args[0].withRelated)
          .to.include('tags');
      });
    });

    describe('when specified post dose not exist', function() {
      beforeEach(function() {
        this.next = sinon.spy();
        this.mockModels.Post.fetchStub.returns({
          then: function(onSuccess, onError) {
            return onError(new Error('EmptyResponse'));
          }
        });
      });

      it('should call next with error', function() {
        post.show(this.req, this.res, this.next);

        expect(this.next).to.have.been.called;
      });

      it('should set status code 404.', function() {
        post.show(this.req, this.res, this.next);

        expect(this.res.statusCode).to.equal(404);
      });
    });
  });
});

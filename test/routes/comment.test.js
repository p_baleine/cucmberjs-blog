var chai = require('chai'),
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    models = require('../../app/models'),
    Bookshelf = require('bookshelf').blogBookshelf,
    comment = require('../../app/routes/comment'),
    fixture = require('../../fixtures').posts,
    expect = chai.expect;

chai.use(sinonChai);

describe('comment route', function() {

  beforeEach(function() {
    this.res = { render: sinon.spy(), redirect: sinon.spy(), send: sinon.spy() };
    this.req = null;
  });

  describe('#create', function() {
    beforeEach(function() {
      this.req = {
        params: { post: 11 },
        body: { commenter: 'miku', body: 'mikumiku' } 
      };
      this.saveStub = sinon.stub(models.Comment.prototype, 'save');
      this.saveStub.returns({
        then: function(cb) {
          cb();
        }
      });
    });

    afterEach(function() {
      models.Comment.prototype.save.restore();
    });

    it('should save passed commenter.', function() {
      comment.create(this.req, this.res);

      expect(this.saveStub).to.have.called;
      expect(this.saveStub.thisValues[0].get('commenter')).to.equal('miku');
    });

    it('should save passed body.', function() {
      comment.create(this.req, this.res);

      expect(this.saveStub).to.have.called;
      expect(this.saveStub.thisValues[0].get('body')).to.equal('mikumiku');
    });

    it('should save model with post\'s id.', function() {
      comment.create(this.req, this.res);

      expect(this.saveStub.thisValues[0].get('post_id')).to.equal(11);
    });

    describe('when the new post is created successfully,', function() {
      it('should send back the created comment.', function() {
        comment.create(this.req, this.res);

        expect(this.res.send).to.have.been.called;
        expect(this.res.send.lastCall.args[0]).to.have.property('post_id', 11);
      });
    });

    describe('when the new post is not created by error', function() {
      beforeEach(function() {
        this.saveStub.returns({
          then: function(onSuccess, onError) {
            onError(new Error('some error'));
          }
        });
      });

      it('should respond with error.', function() {
        comment.create(this.req, this.res);

        expect(this.res.send).to.have.been.called;
        expect(this.res.statusCode).to.equal(422);
        expect(this.res.send.lastCall.args[0]).to.have.property('error', 'some error');
      });
    });
  });
});

var _ = require('underscore'),
    sinon = require('sinon'),
    Bookshelf = require('bookshelf').blogBookshelf,
    BaseModel = require('../../app/models/base'),
    models = require('../../app/models');

module.exports = exports = mockModels;

function mockModels() {
  return new MockModels();
};

function MockModels() {
  _.each(models, function(Model, name) {
    this[name] = {};

    var findAllStub = this[name].findAllStub =
          sinon.stub(models[name], 'findAll');
    findAllStub.returns({
      then: function(cb) {
        return cb(new Bookshelf.Collection());
      }
    });

    var fixture = require('../../fixtures')[name.toLowerCase() +  's'];

    var fetchStub = this[name].fetchStub =
          sinon.stub(models[name].prototype, 'fetch');
    fetchStub.returns({
      then: function(cb) {
        return cb(new Bookshelf.Model(fixture[0]));
      }
    });

    var saveStub = this[name].saveStub =
          sinon.stub(models[name].prototype, 'save');
    saveStub.returns({
      then: function(cb) {
        cb();
      }
    });
    
  }, this);
};

MockModels.prototype.restore = function() {
  _.each(models, function(Model, name) {
    this[name].findAllStub.restore();
    this[name].fetchStub.restore();
    this[name].saveStub.restore();
  }, this);
};

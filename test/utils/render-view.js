var jsdom = require('jsdom'),
    path = require('path'),
    readFileSync = require('fs').readFileSync,
    Promise = require('bluebird'),
    _ = require('underscore'),
    viewHelpers = require('../../app/view-helpers'),
    jade = require('jade');

var jquery = readFileSync(path.join(__dirname, '../../vendor/jquery/jquery.min.js'));

module.exports = exports = function(fileName, local) {
  var deferred = Promise.defer(),
      html = jade.renderFile(fileName, _.extend({ viewHelpers: viewHelpers }, local));

  jsdom.env({
    html: html,
    src: [jquery],
    done: function(err, window) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(window);
      }
    }
  });

  return deferred.promise;
};

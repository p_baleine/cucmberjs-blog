
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('tags', function(t) {
      t.increments().primary();
      t.string('text').notNull();
      t.dateTime('created_at').notNull();
      t.dateTime('updated_at').nullable();
    }),

    knex.schema.createTable('posts_tags', function(t) {
      t.increments().primary();
      t.integer('post_id').notNull().references('id').inTable('posts');
      t.integer('tag_id').notNull().references('id').inTable('tags');
      t.unique(['post_id', 'tag_id'], 'post_tag_index');
    }),

    knex('tags').insert(require('../../fixtures').tags.map(function(tag) {
      return { text: tag.text, created_at: new Date() };
    }))
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('tags'),
    knex.schema.dropTable('posts_tags')
  ]);
};

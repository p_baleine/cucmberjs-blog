
exports.up = function(knex, Promise) {
  return knex.schema.createTable('comments', function(t) {
    t.increments().primary();
    t.integer('post_id').notNull().references('id').inTable('posts');
    t.string('commenter').notNull();
    t.string('body').notNull();
    t.dateTime('created_at').notNull();
    t.dateTime('updated_at').nullable();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('comments');
};

module.exports = function() {
  this.Before(function(callback) {
    this.bootServer();
    this.loadFixture(callback);
  });

  this.After(function(callback) {
    this.shutdownServer(callback);
  });
};

var Zombie = require('zombie'),
    url = require('url'),
    loadFixture = require('./load-fixture');

// Expose World.
exports.World = World;

// port for target app.
var PORT = 3001;

/**
 * World constructor.
 *
 * @param {Function} callback
 */

function World(callback) {
  this.app = require('../../app');
  this.server = null;
  this.browser = new Zombie();
  callback();
};

/**
 * Visit the specified page.
 *
 * @param {String} path
 * @param {Function} callback
 */

World.prototype.visit = function(path, callback) {
  this.browser.visit(url.format({
    protocol:'http',
    hostname: 'localhost',
    port: PORT,
    pathname: path
  }), callback);
};

/**
 * Boot server.
 *
 * @param {Function} callback
 */

World.prototype.bootServer = function() {
  this.server = this.app.listen(PORT);
};

/**
 * Shutdown server.
 *
 * @param {Function} callback
 */

World.prototype.shutdownServer = function(callback) {
  this.server.close(callback);
};

/**
 * Load fixture data.
 *
 * @param {Function} callback
 */

World.prototype.loadFixture = function(callback) {
  loadFixture(callback);
};


var Promise = require('bluebird'),
    knex = require('knex').initialize(require('config').db),
    fixture = require('../../fixtures');

// Expose `load`.
module.exports = load;

function load(callback) {
  // TODO abstract reset of sequence
  return Promise.all([
    knex('posts').del(),
    knex('sqlite_sequence').where('name', 'posts').update({ seq: 0 }),
    knex('posts').insert(fixture.posts),
    knex('comments').del(),
    knex('sqlite_sequence').where('name', 'comments').update({ seq: 0 }),
    knex('comments').insert(fixture.comments),
    knex('tags').del(),
    knex('sqlite_sequence').where('name', 'tags').update({ seq: 0 }),
    knex('tags').insert(fixture.tags),
    knex('posts_tags').del(),
    knex('sqlite_sequence').where('name', 'posts_tags').update({ seq: 0 }),
    knex('posts_tags').insert(fixture.posts_tags)
  ]).then(callback, function() { console.log(arguments); });
}

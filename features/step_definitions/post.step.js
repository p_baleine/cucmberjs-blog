var expect = require('chai').expect,
    World = require('../support/world').World,
    fixture = require('../../fixtures');

module.exports = function() {
  this.World = World;

  this.Given(/^サンプルアプリの投稿一覧にアクセスする$/, function(callback) {
    this.visit('/posts', callback);
  });

  this.Given(/^サンプルアプリの投稿新規作成画面にアクセスする$/, function(callback) {
    this.visit('/posts/new', callback);
  });

  this.Given(/^新しい投稿のタイトルと内容を入力して作成ボタンをクリックする$/, function(callback) {
    this.browser
      .fill('post[title]', 'タイトル')
      .fill('post[description]', '新しい投稿の内容')
      .pressButton('[type=submit]', callback);
  });

  this.Given(/^サンプルアプリの先頭の投稿の編集画面にアクセスする$/, function(callback) {
    this.visit('/posts/1/edit', callback);
  });

  this.When(/^新規作成のリンクをクリックする$/, function(callback) {
    this.browser.clickLink('[href="/posts/new"]', callback);
  });

  this.When(/^先頭の投稿のリンクをクリックする$/, function(callback) {
    this.browser.clickLink('.post:first-child a', callback);
  });

  this.When(/^投稿のタイトルと内容を変更して作成ボタンをクリックする$/, function(callback) {
    this.browser
      .fill('post[title]', 'タイトル変更済')
      .fill('post[description]', '投稿の内容タイトル変更済')
      .pressButton('[type=submit]', callback);
  });

  this.Then(/^投稿一覧が表示されること$/, function(callback) {
    expect(this.browser.query('ul#posts')).to.exist;
    callback();
  });

  this.Then(/^投稿一覧に遷移すること$/, function(callback) {
    expect(this.browser.location).to.match(/\/posts$/);
    callback();
  });

  this.Then(/^新規作成のリンクが表示されること$/, function(callback) {
    expect(this.browser.query('a[href*="/posts/new"]')).to.exist;
    callback();
  });

  this.Then(/^投稿詳細へのリンクが表示されること$/, function(callback) {
    expect(this.browser.query('.post:first-child a[href*="/posts/1"]')).to.exist;
    callback();
  });

  this.Then(/^投稿新規作成画面が表示されること$/, function(callback) {
    expect(this.browser.location).to.match(/\/posts\/new$/);
    callback();
  });

  this.Then(/^投稿新規作成用のフォームが表示されること$/, function(callback) {
    var form = this.browser.query('form');

    expect(form).to.exist;
    expect(form.action).to.equal('/posts');
    expect(form.method).to.equal('post');
    callback();
  });

  this.Then(/^コメントの新規作成用のフォームが表示されること$/, function(callback) {
    var form = this.browser.query('form'),
        post = fixture.posts[0];

    expect(form).to.exist;
    expect(form.action).to.equal('/posts/' + post.id + '/comments');
    expect(form.method).to.equal('POST');
    callback();
  });

  this.Then(/^投稿一覧の一番下に今作成した投稿が表示されること$/, function(callback) {
    expect(this.browser.text('.post:last-child .title')).to.equal('タイトル');
    expect(this.browser.text('.post:last-child .description')).to.equal('新しい投稿の内容');
    callback();
  });

  this.Then(/^先頭の投稿の詳細が表示されること$/, function(callback) {
    var post = fixture.posts[0];

    expect(this.browser.text('.title')).to.equal(post.title);
    expect(this.browser.text('.description')).to.equal(post.description);
    callback();
  });

  this.Then(/^先頭の投稿のコメント一覧が表示されること$/, function(callback) {
    expect(this.browser.queryAll('.comment')).to.have.length(fixture.comments.length);
    callback();
  });

  this.Then(/^登録済みのタグのチェックボックスが表示されること$/, function(callback) {
    expect(this.browser.query('form .tags label:first-child').textContent)
      .to.equal(fixture.tags[0].text);
    expect(this.browser.query('form .tags [name="post[tags]"]:first-child').value)
      .to.equal(fixture.tags[0].id + '');
    callback();
  });

  this.Then(/^各投稿にタグが表示されること$/, function(callback) {
    expect(this.browser.queryAll('.post:first-child .tags .tag'))
      .to.have.length(fixture.posts_tags.length);
    callback();
  });

  this.Then(/^投稿にタグが表示されること$/, function(callback) {
    expect(this.browser.queryAll('.tags li')).to.have.length(fixture.posts_tags.length);
    callback();
  });

  this.Then(/^先頭の投稿の編集のリンクが表示されること$/, function(callback) {
    expect(this.browser.query('a[href="/posts/1/edit"]')).to.exist;
    callback();
  });

  this.Then(/^先頭の投稿のタイトルは変更されていること$/, function(callback) {
    expect(this.browser.text('.post:first-child .title')).to.equal('タイトル変更済');
    callback();
  });
};

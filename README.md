# cucumberjs-blog [![Build Status](https://drone.io/bitbucket.org/p_baleine/cucmberjs-blog/status.png)](https://drone.io/bitbucket.org/p_baleine/cucmberjs-blog/latest)

Sample app for cucumberjs.

## Development

```bash
# clone app
$ git clone https://p_baleine@bitbucket.org/p_baleine/cucmberjs-blog.git
$ cd cucmberjs-blog
# install dependencies
$ npm install
# setup databse
$ grunt migrate:latest
# start server
$ grunt server
```

var Validator = require('./base');

/**
 * Constructor for post validator.
 */

function PostValidator() {
  Validator.apply(this, arguments);
}

// inherit Validator
require('inherits')(PostValidator, Validator);

PostValidator.prototype.validate = function(model) {
  this.check(model.get('title'), 'Title can not be blank.').notEmpty();

  return this;
};

module.exports = PostValidator;

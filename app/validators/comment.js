var Validator = require('./base');

/**
 * Constructor for comment validator.
 */

function CommentValidator() {
  Validator.apply(this, arguments);
}

// inherit Validator
require('inherits')(CommentValidator, Validator);

/**
 * @Override
 */

CommentValidator.prototype.validate = function(model) {
  this.check(model.get('commenter'), 'Commenter can not be blank.').notEmpty();

  return this;
};

module.exports = CommentValidator;

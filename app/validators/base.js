var _ = require('underscore'),
    Validator = require('validator').Validator;

/**
 * Custom validator constructor.
 */

function BlogValidator() {
  Validator.apply(this, arguments);
}

// inherit Validator
require('inherits')(BlogValidator, Validator);

/**
 * @Override
 */

BlogValidator.prototype.error = function(msg) {
  this._errors.push(msg);
  return this;
};

/**
 * Get errors.
 */

BlogValidator.prototype.getErrors = function() {
  return this._errors;
};

/**
 * Return true if this has errors.
 */

BlogValidator.prototype.hasErrors = function() {
  return !_.isEmpty(this.getErrors());
};

/**
 * validate,
 * this method should be override.
 */

BlogValidator.prototype.validate = function() {};

module.exports = BlogValidator;

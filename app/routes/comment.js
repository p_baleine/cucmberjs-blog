var Post = require('../models').Post,
    Comment = require('../models').Comment;

/*
 * POST /posts/:id/comments
 */

exports.create = function(req, res) {
  var comment = new Comment(req.body),
      postId = Number(req.params.post);

  comment.set('post_id', postId);

  comment.save().then(function() {
    res.send(comment.toJSON());
  }, function(e) {
    res.statusCode = 422;
    res.send({ error: e.message });
  });
};

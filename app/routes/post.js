var _ = require('underscore'),
    Post = require('../models').Post,
    Tag = require('../models').Tag,
    ValidatorError = require('validator').ValidatorError;

/*
 * GET /posts
 */

exports.index = function(req, res) {
  Post.findAll({ withRelated: ['comments', 'tags'] })
    .then(function(posts) {
      res.render('post/index', { posts: posts.toJSON() });
    });
};

/*
 * GET /posts/:post
 */

exports.show = function(req, res, next) {
  new Post({ id: Number(req.params.post) })
    .fetch({ withRelated: ['comments', 'tags'], require: true })
    .then(function(post) {
      res.render('post/show', { post: post.toJSON() });
    }, function(e) {
      if (e.message === 'EmptyResponse') { res.statusCode = 404; }
      next(e);
    });
};

/*
 * GET /posts/new
 */

exports.new = function(req, res) {
  Tag.findAll().then(function(tags) {
    res.render('post/new', { post: new Post().toJSON(), tags: tags.toJSON() });
  });
};

/*
 * POST /posts
 */

exports.create = function(req, res, next) {
  var post = new Post(req.body.post);

  post.save()
    .then(function() {
      res.redirect('/posts');
    }, function(e) {
      if (!(e instanceof ValidatorError)) { return next(e); }
      res.statusCode = 422;
      res.render('post/new', { post: post.toJSON(), error: e.message });
    });
};

/*
 * GET /posts/:post/edit
 */

exports.edit = function(req, res, next) {
  Tag.findAll().then(function(tags) {
    new Post({ id: req.params.post })
      .fetch({ withRelated: ['tags'] })
      .then(function(post) {
        res.render('post/edit', { post: post.toJSON(), tags: tags.toJSON() });
      }, function(e) {
        if (e.message === 'EmptyResponse') { res.statusCode = 404; }
        next(e);
      });
  });  
};

/*
 * PUT /posts/:post
 */

exports.update = function(req, res, next) {
  var id = req.params.post,
      post = new Post(_.extend({ id: id }, req.body.post));

  post.save()
    .then(function() {
      res.redirect('/posts');
    }, function(e) {
      if (!(e instanceof ValidatorError)) { return next(e); }
      res.statusCode = 422;
      Tag.findAll().then(function(tags) {
        res.render('post/edit', { post: post.toJSON(), tags: tags.toJSON(), error: e.message });
      });
    });

};


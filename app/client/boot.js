var $ = require('jquery'),
    Backbone = require('backbone'),
    AppView = require('./app-view'),
    Comments = require('./comment/collection');

Backbone.$ = $;

var AppRouter = Backbone.Router.extend({

  routes: {
    '': 'index'
  },

  initialize: function(options) {
    // TODO post id
    this.comments = new Comments(options.comments, { post_id: options.postId });
  },

  index: function() {
    if (!this.appView) {
      this.appView = new AppView({
        el: '#comment-container',
        comments: this.comments
      });
    }
    this.appView.render();
  }

});

$(function(){
  new AppRouter({ comments: window.comments, postId: window.post.id });
  Backbone.history.start();
});

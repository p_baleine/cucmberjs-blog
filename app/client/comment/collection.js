var Backbone = require('backbone'),
    Validator = require('../../validators/comment');

var CommentModel = module.exports = Backbone.Model.extend({

  initialize: function() {
    this.validator = new Validator();
  },

  validate: function(attrs) {
    this.validator.validate(this);
    if (this.validator.hasErrors()) { return this.validator.getErrors(); }
  }

});

var CommentCollection = module.exports = Backbone.Collection.extend({

  model: CommentModel,

  url: function() {
    return '/posts/' + this.postId + '/comments';
  },

  initialize: function(models, options) {
    this.postId = options.post_id;
  }
});

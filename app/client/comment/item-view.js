var Backbone = require('backbone');

var CommentItemView = module.exports = Backbone.View.extend({

  tagName: 'li',

  initialize: function() {
    this.model.on('invalid', this.remove, this);
  },

  render: function() {
    this.$el.html(this.template({ comment: this.model.toJSON() }));
    return this;
  },

  template: require('../../views/comment/comment.jade')

});

var Backbone = require('backbone'),
    ItemView = require('./item-view');

var CommentListView = module.exports = Backbone.View.extend({

  tagName: 'ul',

  initialize: function() {
    this.collection.on('add', this.renderOne, this);
  },

  setChildElements: function() {
    this.$('.comment').each(Backbone.$.proxy(function(idx, elt) {
      var commentId = Number(Backbone.$(elt).data('comment-id')),
          comment = this.collection.get(commentId),
          view = new ItemView({ model: comment });

      view.setElement(elt);
    }, this));
    return this;
  },

  renderOne: function(model) {
    this.$el.append(new ItemView({ model: model }).render().el);
    return this;
  }

});


var Backbone = require('backbone'),
    CommentListView = require('./comment/list-view');

var AppView = module.exports = Backbone.View.extend({

  events: {
    'submit form': 'saveComment'
  },

  initialize: function(options) {
    this.comments = options.comments;
    new CommentListView({
      el: '.comments',
      collection: this.comments
    }).setChildElements(); // set `el` for rendered item views.
  },

  saveComment: function(e) {
    e.preventDefault();

    var comment = this.comments.create({
      commenter: this.$('[name="comment[commenter]"]').val(),
      body: this.$('[name="comment[body]"]').val()
    });

    this.$('.errors').empty();
    if (comment.validationError) { this.renderErrors(comment.validationError); }
  },

  renderErrors: function(errors) {
    var _this = this;

    Backbone.$.each(errors, function() {
      _this.$('.errors').append(_this.errorTemplate({ e: this }));
    });
  },

  errorTemplate: require('../views/error.jade')

});


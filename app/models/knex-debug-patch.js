var colors = require('colors');

module.exports = function(knex) {
  var originalDebug = knex.client.debug;

  knex.client.debug = function(sql, bindings, connection, builder) {
    console.log('  sql: ' + sql.cyan);
    console.log('  bindings: ' + '['.green);
    bindings.forEach(function(binding) {
      console.log('    ' + binding.toString().green);
    });
    console.log('  ]'.green);
    console.log('  __cid: ' + connection.__cid.green);
    console.log('');
  };
};

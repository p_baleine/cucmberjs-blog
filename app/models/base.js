var Bookshelf = require('bookshelf'),
    config = require('config'),
    ValidatorError = require('validator').ValidatorError;

// initialize database
var blogBookshelf = Bookshelf.blogBookshelf = Bookshelf.initialize(config.db);

if (config.db.debug) { require('./knex-debug-patch')(blogBookshelf.knex); }

/**
 * Base model
 */

var BaseModel = module.exports = blogBookshelf.Model.extend({

  initialize: function() {
    this.on('saving', this.validate, this);
  },

  validate: function() {
    this.validator.validate(this);

    if (this.validator.hasErrors()) { throw new ValidatorError(this.validator.getErrors()); }

    return true;
  },

  validator: new require('../validators/base')

}, {

  findAll: function() {
    var coll = blogBookshelf.Collection.forge([], { model: this });
    return coll.fetch.apply(coll, arguments);
  }
});


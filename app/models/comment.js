var BaseModel = require('./base'),
    Bookshelf = require('bookshelf').blogBookshelf,
    Validator = require('../validators/comment');

/**
 * Comment model
 */

var Comment = BaseModel.extend({

  tableName: 'comments',

  hasTimestamps: true,

  initialize: function() {
    BaseModel.prototype.initialize.apply(this, arguments);
    this.validator = new Validator();
  },

  post: function() {
    return this.belongsTo(require('./post').Post);
  }

});

/**
 * Post collection
 */

var Comments = Bookshelf.Collection.extend({
  model: Comment
});

module.exports = {
  Comment: Comment,
  Comments: Comments
};

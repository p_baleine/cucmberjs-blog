var Bookshelf = require('bookshelf'),
    path = require('path');

module.exports = {
  Post: require('./post').Post,
  Comment: require('./comment').Comment,
  Tag: require('./tag').Tag
};

var BaseModel = require('./base'),
    Bookshelf = require('bookshelf').blogBookshelf,
    Post = require('./post').Post;

/**
 * Tag model
 */

var Tag = BaseModel.extend({

  tableName: 'tags',

  hasTimestamps: true,

  posts: function() {
    return this.belongsToMany(Post);
  }

});

/**
 * Post collection
 */

var Tags = Bookshelf.Collection.extend({
  model: Tag
});

module.exports = {
  Tag: Tag,
  Tags: Tags
};

var _ = require('underscore'),
    BaseModel = require('./base'),
    Bookshelf = require('bookshelf').blogBookshelf,
    Validator = require('../validators/post'),
    Comment = require('./comment').Comment,
    Tag = require('./tag').Tag;

/**
 * Post model
 */

var Post = BaseModel.extend({

  tableName: 'posts',

  hasTimestamps: true,

  initialize: function() {
    BaseModel.prototype.initialize.apply(this, arguments);
    this.validator = new Validator();
  },

  comments: function() {
    return this.hasMany(Comment);
  },

  tags: function() {
    return this.belongsToMany(Tag);
  },

  /**
   * @Override
   *
   * save post and tags.
   * only support [params, options] signature.
   */

  save: function(params, options) {
    var _this = this;

    return Bookshelf.transaction(function(t) {
      var tags = _this.get('tags');

      _this.attributes = _this.omit('tags');
      options = _.extend(options || {}, { transacting: t });

      return BaseModel.prototype.save.call(_this, params, options)
        .then(function() {
          // first detach all tags to which this post related
          return _this.tags().detach(null, options);
        })
        .then(function() {
          // then attach passed tags
          return _this.tags().attach(tags, options);
        })
        .then(t.commit, t.rollback);
    }).yield(this);
  }

});

/**
 * Post collection
 */

var Posts = Bookshelf.Collection.extend({
  model: Post
});

module.exports = {
  Post: Post,
  Posts: Posts
};

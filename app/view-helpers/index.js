var _ = require('underscore'),
    moment = require('moment');

exports.formatDate = function(date) {
  return moment(date).format();
};

exports.isPostContainingTag = function(post, tag) {
  return _.contains(post.tags.map(function(tag) { return tag.id; }), tag.id);
};

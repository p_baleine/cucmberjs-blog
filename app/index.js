var express = require('express'),
    Resource = require('express-resource'),
    path = require('path'),
    viewHelpers = require('./view-helpers');

var app = module.exports = express();

// Settings.

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

// Middlewares.

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, '../public')));
// TODO errorHandler for production mode
app.use(express.errorHandler());

app.locals({ viewHelpers: viewHelpers });

// routes

var post = require('./routes/post'),
    comment = require('./routes/comment');

app.get('/', post.index); // root
app.resource('posts', post) // posts
  .add(app.resource('comments', comment)); // posts/comments

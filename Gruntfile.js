module.exports = function(grunt) {

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    cucumberjs: {
      files: 'features'
    },

    mochacov: {
      app: {
        src: ['test/**/*.test.js']
      },
      appcov: {
        options: {
          reporter: 'html-cov',
          output: 'covorage.html'
        },
        src: ['test/**/*.test.js']
      }
    },

    nodemon: {
      dev: {}
    },

    env: {
      dev: {
        NODE_ENV: 'development'
      },
      unit: {
        NODE_ENV: 'unit'
      },
      cucumber: {
        NODE_ENV: 'cucumber'
      }
    },

    clean: {
      unit: ['db/blog-unit.db'],
      cucumber: ['db/blog-cucumber.db']
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: ['Gruntfile.js', 'index.js', 'app/**/*.js']
    },

    browserify: {
      build: {
        files: { 'public/application.js': ['app/client/boot.js'] },
        options: {
          debug: true
        }
      },
      options: {
        transform: ['browserify-jade'],
        shim: {
          jquery: {
            path: 'vendor/jquery/jquery.min.js',
            exports: '$'
          }
        }
      }
    },

    less: {
      build: {
        files: { 'public/application.css': ['less/application.less'] }
      }
    },

    watch: {
      jshint: {
        files: ['<%= jshint.all %>'],
        tasks: ['jshint']
      },
      browserify: {
        files: ['app/client/**/*.js'],
        tasks: ['browserify']
      },
      less: {
        files: ['less/**/*.less']
      },
      mochacov: {
        files: ['<%= jshint.all %>', '<%= mochacov.app.src %>', 'app/**/*.jade'],
        tasks: ['unit']
      }
    }
  });

  grunt.registerTask('build', [
    'browserify',
    'less'
  ]);

  grunt.registerTask('server', [
    'env:dev',
    'build',
    'nodemon'
  ]);

  grunt.registerTask('covorage', [
    'env:unit',
    'build',
    'mochacov:appcov'
  ]);

  grunt.registerTask('unit', [
    'env:unit',
    'clean',
    'build',
    'migrate:latest',
    'mochacov:app'
  ]);

  grunt.registerTask('cucumber', [
    'env:cucumber',
    'clean',
    'build',
    'migrate:latest',
    'browserify',
    'cucumberjs'
  ]);

  grunt.loadTasks('tasks');
};
